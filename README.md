# Proyecto de Administración de Proyectos

Proyecto para la materia de Administración de Proyectos 2015-2 de la Facultad de Ciencias.

## Registro y seguimiento de la opción de titulación por tesis

Proyecto para apoyar a los estudiantes y a algunas entidades administrativas de la Facultad de Estudios
Superiores Acatlán en la realización de los trámites de la titulación por tesis.

El objetivo de este proyecto es unificar todas las actividades que llevan a cabo la Secretaría Técnica de Carrera
(STC), el Departamento de Títulos (DT) y los estudiantes de esa Facultad, durante el proceso de titulación por
tesis. Se pretende hacer esta unificación en un prototipo de sistema basado en web que cuente con capacidad de
almacenamiento y manejo de toda la información necesaria respecto a la opción de titulación por tesis. El sistema
(a partir de ahora nos referiremos al prototipo del sistema simplemente como sistema) debe lograr automatizar
varios procesos que se llevan a cabo actualmente de forma manual. También el sistema deberá mantener al tanto
del estado de cada tesis a todos los interesados por medio de notificiaciones internas y por email.

### Desarrolladores

* Anuar Tapia [anuartap@ciencias.unam.mx](mailto:anuartap@ciencias.unam.mx)
* Mariana Valdivia [marianavc@ciencias.unam.mx](mailto:marianavc@ciencias.unam.mx)
* Rocío Yáñez [monse@ciencias.unam.mx](mailto:monse@ciencias.unam.mx)
* José Vargas [enrique92espartan@hotmail.com](mailto:enrique92espartan@hotmail.com)